const bdb = require("./blocksdb");
const botjs = require("./bot");
const helper = require("./helper");
const LONG_DELAY = 12000;
const SHORT_DELAY = 3000;
const SUPER_LONG_DELAY = 1000 * 60 * 15;

async function processAward(bn, award) {
    let results_count = await botjs.awardMSG(award);
    return results_count;
}

async function processBlock(bn) {
    const block =await helper.getOpsInBlock(bn); 
    let ops_array = [];
    for(let tr of block) {
        const [op, opbody] = tr.op;

        switch(op) {
            case "benefactor_award":
            ops_array.push({type: 'benefactor_award', data: opbody});
break;
case "receive_award":
ops_array.push({type: 'receive_award', data: opbody});
break;
default:
                //неизвестная команда
        }
    }
let ok_ops_count = 0;
if (ops_array.length > 0) {
            ok_ops_count = await processAward(bn, ops_array);
} else {
ok_ops_count = 0;
}
return ok_ops_count;
}

let PROPS = null;

let bn = 0;
let last_bn = 0;
let delay = SHORT_DELAY;

    async function run() {
        PROPS = await helper.getProps();
    
        let db_block = await bdb.getBlock(PROPS.last_irreversible_block_num);
        bn = db_block.last_block;
        
        delay = SHORT_DELAY;
        while (true) {
            try {
                if (bn > PROPS.last_irreversible_block_num) {
                    await new Promise(r => setTimeout(r, delay));
                    PROPS = await helper.getProps();
                } else {
                    if(0 < await processBlock(bn)) {
                        delay = SHORT_DELAY;
                    } else {
                        delay = LONG_DELAY;
                    }
                    bn++;
                    await bdb.updateBlock(bn);
                }
            } catch (e) {
                console.error("error in main loop" + e);
                await new Promise(r => setTimeout(r, 1000));
            }
        }
    }

    setInterval(() => {
        if(last_bn == bn) {
    
            try {
                    process.exit(1);
            } catch(e) {
                process.exit(1);
            }
        }
        last_bn = bn;
    }, SUPER_LONG_DELAY);

async function noReturn() {
    await botjs.startCommand();
    await botjs.aboutCommand();
    await botjs.adminCommand();
    await botjs.subscribeCommand();
    await botjs.unsubscribeCommand();
    await botjs.subscribesCommand();
    await botjs.addStopCommand();
    await botjs.delStopCommand();
await botjs.stoppedCommand();
    await botjs.yesCommand();
    await botjs.supportCommand();
    await botjs.nullSupportCommand();
await botjs.helpCommand();
await botjs.langEngCommand();
await botjs.langRuCommand();
}
    run();
    noReturn()